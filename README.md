# Dynamic Path Rewrites

Change entity type router paths without having to create path aliases. Uses
inbound/outbound processing to rewrite paths on the fly. Rewrites are cached to
keep subsequent requests performant. Varying paths per entity type bundle is
supported. For example, `/article/{node}` for an article and
`/blog-post/{node}` for a blog post. Works with any entity type route!

## Usage

1. Download and install the `drupal/dynamic_path_aliases` module. Recommended install method is composer:
   ```
   composer require drupal/dynamic_path_aliases
   ```
2. Go to /admin/config/search/path/rewrite to add path rewrites.

## Token replacements

Sorry, tokens are NOT supported! Tokens would add too much
performance overhead. This approach is intended to avoid the creation of many
path alias entities while still allowing paths to be easily changed. For
example, on larger sites with many thousands of users or content. If tokens
are absolutely necessary, use the tried and true
https://www.drupal.org/project/pathauto module.
