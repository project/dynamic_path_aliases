<?php

namespace Drupal\dynamic_path_aliases;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * List builder for path rewrite entity.
 *
 * @see \Drupal\dynamic_path_aliases\Entity\PathRewrite
 */
class PathRewriteListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  protected $weightKey = 'weight';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'dynamic_path_aliases_path_rewrite_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    return [
      'label' => $this->t('Label'),
      'type' => $this->t('Type'),
      'path' => $this->t('Path'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\dynamic_path_aliases\PathRewriteInterface $entity */
    return [
      'label' => $entity->label(),
      'type' => [
        '#markup' => $entity->getTargetEntityType()->getLabel(),
      ],
      'path' => [
        '#markup' => $entity->get('path'),
      ],
    ] + parent::buildRow($entity);
  }

}
