<?php

namespace Drupal\dynamic_path_aliases;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Provides an interface defining a path rewrite entity.
 */
interface PathRewriteInterface extends ConfigEntityInterface {

  /**
   * Get the target entity type.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   *   Returns the loaded target entity type.
   */
  public function getTargetEntityType(): EntityTypeInterface;

  /**
   * Get the target entity bundles.
   *
   * @return array
   *   Returns an array of target entity bundles.
   */
  public function getTargetBundle(): array;

  /**
   * Get the route.
   *
   * @param string $routeName
   *   The route name.
   *
   * @return \Symfony\Component\Routing\Route|false
   *   Returns the matching route. Otherwise, FALSE.
   */
  public function getRoute(string $routeName);

  /**
   * Get the route by path.
   *
   * @param string $path
   *   The path.
   *
   * @return \Symfony\Component\Routing\Route|false
   *   Returns the matching route. Otherwise, FALSE.
   */
  public function getRouteByPath(string $path);

}
