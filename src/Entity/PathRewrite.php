<?php

namespace Drupal\dynamic_path_aliases\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\dynamic_path_aliases\PathRewriteInterface;

/**
 * Defines the path rewrite configuration entity.
 *
 * @ConfigEntityType(
 *   id = "path_rewrite",
 *   label = @Translation("Path rewrite"),
 *   label_collection = @Translation("Path rewrites"),
 *   label_singular = @Translation("Path rewrite"),
 *   label_plural = @Translation("Path rewrites"),
 *   label_count = @PluralTranslation(
 *     singular = "@count path rewrite",
 *     plural = "@count path rewrites",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\dynamic_path_aliases\PathRewriteListBuilder",
 *     "form" = {
 *       "default" = "Drupal\dynamic_path_aliases\PathRewriteForm",
 *       "add" = "Drupal\dynamic_path_aliases\PathRewriteForm",
 *       "edit" = "Drupal\dynamic_path_aliases\PathRewriteForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer dynamic path rewrites",
 *   config_prefix = "dynamic_path_aliases",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/search/path/rewrite/add",
 *     "edit-form" = "/admin/config/search/path/rewrite/manage/{path_rewrite}",
 *     "delete-form" = "/admin/config/search/path/rewrite/manage/{path_rewrite}/delete",
 *     "collection" = "/admin/config/search/path/rewrite"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "entity_type",
 *     "bundle",
 *     "route_name",
 *     "path",
 *     "weight",
 *   }
 * )
 */
class PathRewrite extends ConfigEntityBase implements PathRewriteInterface {

  /**
   * The entity type.
   *
   * @var string
   */
  protected string $entity_type;

  /**
   * The bundles.
   *
   * @var array
   */
  protected array $bundle;

  /**
   * The route name.
   *
   * @var string
   */
  protected string $route_name;

  /**
   * The path.
   *
   * @var string
   */
  protected string $path;

  /**
   * The weight.
   *
   * @var int
   */
  protected int $weight = 0;

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityType(): EntityTypeInterface {
    return $this->entityTypeManager()->getDefinition($this->entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetBundle(): array {
    return $this->bundle ?: [$this->entity_type];
  }

  /**
   * {@inheritdoc}
   */
  public function getRoute(string $routeName) {
    /** @var \Drupal\Core\Routing\RouteProviderInterface $routeProvider */
    $routeProvider = \Drupal::service('router.route_provider');
    try {
      return $routeProvider->getRouteByName($routeName);
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * Get the route by path.
   *
   * @param string $path
   *   The path.
   *
   * @return \Symfony\Component\Routing\Route|false
   *   Returns the matching route. Otherwise, FALSE.
   */
  public function getRouteByPath(string $path) {
    /** @var \Drupal\Core\Routing\RouteProviderInterface $routeProvider */
    $routeProvider = \Drupal::service('router.route_provider');
    foreach ($routeProvider->getRoutesByPattern($path) as $route) {
      if ($route->getPath() === $path) {
        return $route;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getListCacheTagsToInvalidate(): array {
    return array_merge(
      parent::getListCacheTagsToInvalidate(),
      $this->getTargetEntityType()->getListCacheTags(),
      ['local_task']
    );
  }

}
