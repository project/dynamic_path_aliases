<?php

namespace Drupal\dynamic_path_aliases;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dynamic_path_aliases\Entity\PathRewrite;

/**
 * The path rewrite entity form.
 *
 * @property \Drupal\dynamic_path_aliases\PathRewriteInterface $entity
 */
class PathRewriteForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#size' => 30,
      '#required' => TRUE,
      '#default_value' => $this->entity->label(),
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('ID'),
      '#machine_name' => [
        'exists' => [PathRewrite::class, 'load'],
        'source' => ['label'],
      ],
      '#maxlength' => 64,
      '#disabled' => !$this->entity->isNew(),
      '#default_value' => $this->entity->id(),
    ];

    // Select an entity type to create route parameters.
    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => [],
      '#default_value' => $this->entity->get('entity_type'),
      '#disabled' => !$this->entity->isNew(),
      '#required' => TRUE,
      '#ajax' => [
        'wrapper' => 'edit-configuration',
        'callback' => [self::class, 'ajax'],
      ],
    ];
    $definitions = [];
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if ($definition instanceof ContentEntityTypeInterface) {
        $definitions[$definition->id()] = $definition;
        $form['entity_type']['#options'][$definition->id()] = $definition->getLabel();
      }
    }

    // Select bundles and configure the path rewrite.
    $form['configuration'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'edit-configuration',
        'class' => !$this->entity->get('entity_type') ? ['hidden'] : [],
      ],
    ];
    $configuration = &$form['configuration'];
    $configuration['bundle'] = [
      '#type' => 'checkboxes',
      '#options' => [],
      '#default_value' => $this->entity->get('bundle') ? $this->entity->get('bundle') : [],
    ];
    $configuration['route_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Route name'),
      '#description' => $this->t('Route to rewrite its path. Search by route name or path.'),
      '#autocomplete_route_name' => 'dynamic_path_aliases.autocomplete',
      '#required' => TRUE,
      '#default_value' => $this->entity->get('route_name'),
    ];
    $configuration['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#required' => TRUE,
      '#default_value' => $this->entity->get('path'),
    ];

    if ($this->entity->get('entity_type') && ($entity = $definitions[$this->entity->get('entity_type')])) {
      // Add options if the entity type has bundles. Otherwise, the entity
      // type itself is considered to be the bundle.
      if ($entity->getBundleEntityType()) {
        $configuration['bundle']['#title'] = $entity->getBundleLabel();
        $configuration['bundle']['#required'] = TRUE;
        foreach ($this->entityTypeManager->getStorage($entity->getBundleEntityType())->loadMultiple() as $bundle) {
          $configuration['bundle']['#options'][$bundle->id()] = $bundle->label();
        }
      }

      $param = "{{$entity->id()}}";
      $configuration['path']['#description'] = $this->t('New route path. Must include the @param route parameter. Example: @list', [
        '@param' => $param,
        '@list' => "/new/path/$param",
      ]);
    }

    return $form;
  }

  /**
   * Ajax callback to replace configuration elements.
   */
  public static function ajax(array &$form, $form_state): array {
    return $form['configuration'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $configuration = &$form['configuration'];
    $entity = $this->entity->getTargetEntityType();

    // Verify the route exists and the path rewrite. Path must include the
    // entity slug and cannot be in use by another route.
    if (!$this->entity->getRoute($form_state->getValue('route_name'))) {
      $form_state->setError($configuration['route_name'], $this->t('Route does not exist.'));
    }
    $path = $form_state->getValue('path');
    $param = "{{$entity->id()}}";
    if (!preg_match('/^\//', $path)) {
      $form_state->setError($configuration['path'], $this->t('Path must start with a forward slash.'));
    }
    elseif (!UrlHelper::isValid(preg_replace("/$param/i", '', $path))) {
      $form_state->setError($configuration['path'], $this->t('Path includes non-valid characters.'));
    }
    elseif (mb_strpos($path, $param) === FALSE) {
      $form_state->setError($configuration['path'], $this->t('Missing the @param route parameter.', [
        '@param' => $param,
      ]));
    }
    elseif ($this->entity->getRouteByPath($path)) {
      $form_state->setError($configuration['path'], $this->t('Path is in use by another route.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->messenger()->addStatus($this->t('Your settings have been saved.'));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }

}
