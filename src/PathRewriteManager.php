<?php

namespace Drupal\dynamic_path_aliases;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\dynamic_path_aliases\Entity\PathRewrite;

/**
 * Provides path rewrite manager.
 */
class PathRewriteManager implements PathRewriteManagerInterface {

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * DynamicPathsManager constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(CacheBackendInterface $cache, EntityTypeManagerInterface $entityTypeManager) {
    $this->cache = $cache;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function match(string $path, string $return): ?string {
    $source = $return === 'path' ? 'rewrite' : 'path';
    if (($cache = $this->getCache("$source:$return:$path")) !== FALSE) {
      return $cache;
    }
    $returnPath = NULL;
    $tags = [];

    // Attempt to match path to a path rewrite configuration. Stop processing
    // soon as a match is found. All non-matches and matches are cached.
    $rewrites = $this->entityTypeManager->getStorage('path_rewrite')->loadMultiple();
    uasort($rewrites, [PathRewrite::class, 'sort']);
    foreach ($rewrites as $rewrite) {
      /** @var \Drupal\dynamic_path_aliases\PathRewriteInterface $rewrite */
      if (!($route = $rewrite->getRoute($rewrite->get('route_name')))) {
        continue;
      }
      $definition = [
        'path' => $route->getPath(),
        'rewrite' => $rewrite->get('path'),
      ];
      $pattern = preg_replace('/\\\\\{[\w_]+\\\}/', '([^\/]+)', preg_quote($definition[$source], '/'));
      if (!preg_match("/^$pattern$/i", $path, $params)) {
        continue;
      }

      // Combine expected route slugs with matched parameters. All slugs
      // in the definition must have matching parameters.
      preg_match('/\{[\w_]+}/', $definition[$source], $slugs);
      array_walk($slugs, function (&$slug): void {
        $slug = str_replace(['{', '}'], '', $slug);
      });
      array_shift($params);
      $params = array_combine($slugs, $params);
      if (count(array_filter($params)) !== count($slugs)) {
        continue;
      }

      // Verify the entity route parameter matches the configuration. Assumes
      // all other route parameters are valid.
      $compiledPath = $definition[$return];
      $bundles = $rewrite->getTargetBundle();
      foreach ($params as $param => $value) {
        if ($param === $rewrite->get('entity_type')) {
          if (($entity = $this->entityTypeManager->getStorage($param)->load($value)) && in_array($entity->bundle(), $bundles, TRUE)) {
            $compiledPath = str_replace("{{$param}}", $value, $compiledPath);
            $tags = array_merge($tags, $entity->getCacheTags());
          }
          else {
            // Unable to load entity or bundle did not match.
            continue 2;
          }
        }
        else {
          $compiledPath = str_replace("{{$param}}", $value, $compiledPath);
        }
      }

      $returnPath = $compiledPath;
    }

    return $this->setCache("$source:$return:$path", $returnPath, $tags);
  }

  /**
   * {@inheritdoc}
   */
  public function getPathByRewrite(string $path): ?string {
    return $this->match($path, 'path');
  }

  /**
   * {@inheritdoc}
   */
  public function getRewriteByPath(string $path): ?string {
    return $this->match($path, 'rewrite');
  }

  /**
   * {@inheritdoc}
   */
  public function getCache(string $key) {
    if ($cache = $this->cache->get($key)) {
      return $cache->data;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setCache(string $key, ?string $value, array $tags = []): ?string {
    $this->cache->set($key, $value, Cache::PERMANENT, array_merge($tags, ['config:path_rewrite_list']));
    return $value;
  }

}
