<?php

namespace Drupal\dynamic_path_aliases\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\dynamic_path_aliases\PathRewriteManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides path rewrite inbound and outbound processor.
 */
class PathRewriteProcessor implements InboundPathProcessorInterface, OutboundPathProcessorInterface {

  /**
   * The path rewrite manager.
   *
   * @var \Drupal\dynamic_path_aliases\PathRewriteManagerInterface
   */
  protected PathRewriteManagerInterface $pathRewriteManager;

  /**
   * PathRewriteProcessor constructor.
   *
   * @param \Drupal\dynamic_path_aliases\PathRewriteManagerInterface $pathRewriteManager
   *   The path rewrite manager.
   */
  public function __construct(PathRewriteManagerInterface $pathRewriteManager) {
    $this->pathRewriteManager = $pathRewriteManager;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request): string {
    if ($sourcePath = $this->pathRewriteManager->getPathByRewrite($path)) {
      return $sourcePath;
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL): string {
    if ($alias = $this->pathRewriteManager->getRewriteByPath($path)) {
      return $alias;
    }
    return $path;
  }

}
