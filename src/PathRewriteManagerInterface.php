<?php

namespace Drupal\dynamic_path_aliases;

/**
 * Provides interface for path rewrite manager.
 */
interface PathRewriteManagerInterface {

  /**
   * Match path rewrite.
   *
   * @param string $path
   *   The path.
   * @param string $return
   *   The type of path to return. Accepts path or rewrite.
   *
   * @return string
   *   Returns the matched path.
   */
  public function match(string $path, string $return): ?string;

  /**
   * Get path by rewrite.
   *
   * @param string $path
   *   The path.
   *
   * @return string
   *   Returns the path.
   */
  public function getPathByRewrite(string $path): ?string;

  /**
   * Get rewrite by path.
   *
   * @param string $path
   *   The path.
   *
   * @return string
   *   Returns the rewrite path.
   */
  public function getRewriteByPath(string $path): ?string;

  /**
   * Get path rewrite cache.
   *
   * @param string $key
   *   The cache key.
   *
   * @return string|false
   *   Returns the cached value. Otherwise, FALSE.
   */
  public function getCache(string $key);

  /**
   * Set path rewrite cache.
   *
   * @param string $key
   *   The cache key.
   * @param string|null $value
   *   The value to cache.
   * @param array $tags
   *   The cache tags.
   *
   * @return string|null
   *   Returns the cached value.
   */
  public function setCache(string $key, ?string $value, array $tags = []): ?string;

}
